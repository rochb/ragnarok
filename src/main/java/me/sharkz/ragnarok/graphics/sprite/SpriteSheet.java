package me.sharkz.ragnarok.graphics.sprite;

import lombok.Data;

import java.awt.image.BufferedImage;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class SpriteSheet {

    private final Sprite sprite;
    private final int width;
    private final int height;
    private final Sprite[][] spriteArray;

    public static final int DEFAULT_SIZE = 32;

    public SpriteSheet(Sprite sprite, int width, int height) {
        this.sprite = sprite;
        this.width = width;
        this.height = height;
        this.spriteArray = loadSpriteArray();
    }

    public SpriteSheet(Sprite sprite) {
        this.sprite = sprite;
        this.width = DEFAULT_SIZE;
        this.height = DEFAULT_SIZE;
        this.spriteArray = loadSpriteArray();
    }

    private Sprite[][] loadSpriteArray() {
        Sprite[][] array = new Sprite[getSpriteHeight()][getSpriteWidth()];
        for (int y = 0; y < getSpriteHeight(); y++)
            for (int x = 0; x < getSpriteWidth(); x++)
                spriteArray[y][x] = getSprite(x, y);
        return array;
    }

    public Sprite getSprite(int x, int y) {
        return this.sprite.getSub(x * width, y * height, width, height);
    }

    public Sprite getNewSprite(int x, int y) {
        return this.sprite.getNew(x * width, y * height, width, height);
    }

    public Sprite getSprite(int x, int y, int w, int h) {
        return this.sprite.getSub(x * w, y * h, w, h);
    }

    public BufferedImage getSubImage(int x, int y, int w, int h) {
        return this.sprite.getImage().getSubimage(x, y, w, h);
    }

    public int getSpriteWidth() {
        return sprite.getWidth() / width;
    }

    public int getSpriteHeight() {
        return sprite.getHeight() / height;
    }
}
