package me.sharkz.ragnarok.graphics.sprite;

import lombok.Data;
import lombok.NonNull;

import java.awt.image.BufferedImage;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class Sprite {

    private final BufferedImage image;
    private final int width;
    private final int height;

    public Sprite(@NonNull BufferedImage image) {
        this.image = image;
        this.width = image.getWidth();
        this.height = image.getHeight();
    }

    public Sprite getSub(int x, int y, int w, int h) {
        return new Sprite(image.getSubimage(x, y, w, h));
    }

    public Sprite getNew(int x, int y, int w, int h) {
        BufferedImage temp = image.getSubimage(x, y, w, h);
        BufferedImage newImage = new BufferedImage(image.getColorModel(), image.getRaster().createCompatibleWritableRaster(w, h), image.isAlphaPremultiplied(), null);
        temp.copyData(newImage.getRaster());
        return new Sprite(newImage);
    }

    public Sprite getNew() {
        return getNew(0, 0, this.width, this.height);
    }
}
