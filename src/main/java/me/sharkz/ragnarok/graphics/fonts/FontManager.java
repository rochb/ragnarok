package me.sharkz.ragnarok.graphics.fonts;

import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class FontManager {

    private final List<Font> fonts = new ArrayList<>();

    public void register(@NonNull Font font) {
        this.fonts.add(font);
    }

    public void unregister(@NonNull Font font) {
        this.fonts.remove(font);
    }

    public Optional<Font> getByName(@NonNull String name) {
        return fonts.stream()
                .filter(font -> font.getName().equalsIgnoreCase(name))
                .findFirst();
    }
}
