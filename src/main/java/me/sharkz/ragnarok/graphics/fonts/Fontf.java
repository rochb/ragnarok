package me.sharkz.ragnarok.graphics.fonts;

import me.sharkz.ragnarok.utils.Resources;

import java.awt.Font;
import java.awt.*;
import java.util.HashMap;
import java.util.Objects;

public class Fontf {

    private static final HashMap<String, Font> fonts = new HashMap<>();

    public static void loadFont(String path, String name) {
        try {
            Font customFont = Font.createFont(Font.TRUETYPE_FONT, Resources.getFontFile(path));
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(customFont);

            Font font = new Font(name, Font.PLAIN, 32);

            fonts.put(name, font);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Font getFont(String name) {
        return fonts.get(name);
    }

}