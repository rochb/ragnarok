package me.sharkz.ragnarok.graphics.fonts;

import lombok.Getter;
import lombok.NonNull;
import me.sharkz.ragnarok.graphics.sprite.Sprite;
import me.sharkz.ragnarok.math.Vector2f;
import me.sharkz.ragnarok.utils.Resources;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

@Getter
public class Font {

    private final String name;
    private final BufferedImage fontSheet;
    private final int width;
    private final int height;
    private final int letterWidth;
    private final int letterHeight;

    public Font(@NonNull String name, @NonNull String path) {
        int TILE_SIZE = 32;
        this.name = name;
        this.width = TILE_SIZE;
        this.height = TILE_SIZE;

        this.fontSheet = Resources.getFont(path);

        assert fontSheet != null;
        this.letterWidth = fontSheet.getWidth() / width;
        this.letterHeight = fontSheet.getHeight() / height;
    }

    public Font(@NonNull String name, @NonNull String path, int width, int height) {
        this.width = width;
        this.height = height;
        this.name = name;

        this.fontSheet = Resources.getFont(path);

        assert fontSheet != null;
        this.letterWidth = fontSheet.getWidth() / width;
        this.letterHeight = fontSheet.getHeight() / height;
    }

    public BufferedImage getLetter(int x, int y) {
        return this.fontSheet.getSubimage(x * width, y * height, width, height);
    }

    public BufferedImage getLetter(char letter) {
        int x = (int) letter % letterWidth;
        int y = (int) letter / letterHeight;
        return getLetter(x, y);
    }

    public static void drawWord(Graphics2D g, ArrayList<Sprite> img, Vector2f pos, int width, int height, int xOffset, int yOffset) {
        for (Sprite sprite : img) {
            if (sprite != null) g.drawImage(sprite.getImage(), pos.getX(), pos.getY(), width, height, null);

            pos.increment(xOffset, yOffset);
        }
    }

    public void drawWord(Graphics2D g, String word, Vector2f pos, int size) {
        drawWord(g, word, pos, size, size, size, 0);
    }

    public void drawWord(Graphics2D g, String word, Vector2f pos, int size, int xOffset) {
        drawWord(g, word, pos, size, size, xOffset, 0);
    }

    public void drawWord(Graphics2D g, String word, Vector2f pos, int width, int height, int xOffset) {
        drawWord(g, word, pos, width, height, xOffset, 0);
    }

    public void drawWord(@NonNull Graphics2D g, @NonNull String word, @NonNull Vector2f pos, @NonNull int width, @NonNull int height, @NonNull int xOffset, @NonNull int yOffset) {
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) != 32)
                g.drawImage(getLetter(word.charAt(i)), pos.getX(), pos.getY(), width, height, null);

            pos.increment(xOffset, yOffset);
        }

    }
}
