package me.sharkz.ragnarok;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class Launcher {

    public static void main(String[] args) {
        new Ragnarok(args);
    }
}
