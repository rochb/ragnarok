package me.sharkz.ragnarok.game;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum GameState {
    MENU,
    PLAYING,
    OPTIONS,
    EASTER_EGG
    ;

}
