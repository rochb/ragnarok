package me.sharkz.ragnarok.game.states;

import lombok.NonNull;
import me.sharkz.ragnarok.Ragnarok;
import me.sharkz.ragnarok.game.GameManager;
import me.sharkz.ragnarok.game.GameState;
import me.sharkz.ragnarok.graphics.fonts.Fontf;
import me.sharkz.ragnarok.inputs.KeyboardHandler;
import me.sharkz.ragnarok.inputs.MouseHandler;
import me.sharkz.ragnarok.math.Vector2f;
import me.sharkz.ragnarok.ui.Button;
import me.sharkz.ragnarok.ui.ButtonManager;
import me.sharkz.ragnarok.ui.ButtonSize;
import me.sharkz.ragnarok.ui.ButtonStyle;
import me.sharkz.ragnarok.utils.Resources;

import java.awt.*;


/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class MenuState implements State {

    public MenuState(@NonNull Ragnarok ragnarok, @NonNull GameManager manager) {
        ButtonManager btnManager = ragnarok.getButtonManager();

        /* Buttons */
        ButtonStyle style = btnManager.getByName("large").orElseThrow(() -> new NullPointerException("Cannot find button style!"));
        btnManager.add(new Button("Play", new ButtonSize(150, 75), new Vector2f(630, 400), style)
                .setClickAction(button -> manager.setState(GameState.PLAYING)));
        btnManager.add(new Button("Options", new ButtonSize(150, 75), new Vector2f(630, 475), style)
                .setClickAction(button -> manager.setState(GameState.OPTIONS)));
        btnManager.add(new Button("Quit", new ButtonSize(150, 75), new Vector2f(630, 550), style)
                .setClickAction(button -> {
                    System.exit(0);
                    // TODO: do a cleaner shutdown
                }));
    }

    @Override
    public void update(double time) {

    }

    @Override
    public void render(Graphics2D g) {
        Image img = Resources.getImage("icon.png");
        assert img != null;
        g.drawImage(img.getScaledInstance(128, 128, 0), 640, 244, null);

        g.setColor(Color.WHITE);
        g.setFont(Fontf.getFont("MinimalPixel").deriveFont(Font.PLAIN, 26));
        g.drawString("Alpha 1.1", 60, 750);
        g.drawString("Developed by Roch Blondiaux", 1200, 750);
    }

    @Override
    public void input(@NonNull MouseHandler handler, @NonNull KeyboardHandler keyboardHandler) {

    }

}
