package me.sharkz.ragnarok.game.states;

import lombok.NonNull;
import me.sharkz.ragnarok.Ragnarok;
import me.sharkz.ragnarok.game.GameManager;
import me.sharkz.ragnarok.game.GameState;
import me.sharkz.ragnarok.graphics.fonts.Fontf;
import me.sharkz.ragnarok.inputs.KeyboardHandler;
import me.sharkz.ragnarok.inputs.MouseHandler;
import me.sharkz.ragnarok.inputs.controls.EasterEggControl;
import me.sharkz.ragnarok.inputs.controls.EscapeControl;

import java.awt.*;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class EasterEggState implements State {

    public EasterEggState(@NonNull Ragnarok ragnarok, @NonNull GameManager manager) {
        ragnarok.getKeyboardHandler()
                .register(new EscapeControl(manager, GameState.MENU));
    }

    @Override
    public void update(double time) {

    }

    @Override
    public void render(Graphics2D g) {
        g.setColor(Color.WHITE);
        g.setFont(Fontf.getFont("MinimalPixel").deriveFont(Font.PLAIN, 80));

        g.drawString("Easter Egg", 660, 140);
    }

    @Override
    public void input(@NonNull MouseHandler handler, @NonNull KeyboardHandler keyboardHandler) {

    }
}
