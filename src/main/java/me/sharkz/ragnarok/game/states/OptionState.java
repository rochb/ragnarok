package me.sharkz.ragnarok.game.states;

import lombok.NonNull;
import me.sharkz.ragnarok.Ragnarok;
import me.sharkz.ragnarok.game.GameManager;
import me.sharkz.ragnarok.game.GameState;
import me.sharkz.ragnarok.graphics.fonts.Fontf;
import me.sharkz.ragnarok.inputs.KeyboardHandler;
import me.sharkz.ragnarok.inputs.MouseHandler;
import me.sharkz.ragnarok.inputs.controls.EasterEggControl;
import me.sharkz.ragnarok.inputs.controls.EscapeControl;
import me.sharkz.ragnarok.math.Vector2f;
import me.sharkz.ragnarok.ui.Button;
import me.sharkz.ragnarok.ui.ButtonManager;
import me.sharkz.ragnarok.ui.ButtonSize;
import me.sharkz.ragnarok.ui.ButtonStyle;
import me.sharkz.ragnarok.utils.ImageUtils;
import me.sharkz.ragnarok.utils.Resources;

import java.awt.*;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class OptionState implements State {

    public OptionState(@NonNull Ragnarok ragnarok, @NonNull GameManager manager) {
        ButtonManager btnManager = ragnarok.getButtonManager();
        ragnarok.getKeyboardHandler()
                .register(new EasterEggControl(manager),
                        new EscapeControl(manager, GameState.MENU));

        ButtonStyle style = btnManager.getByName("square").orElseThrow(() -> new NullPointerException("Cannot find button style!"));
        btnManager.add(new Button(ImageUtils.resize(Resources.getIcon("home.png"), 35, 27), new ButtonSize(75, 75), new Vector2f(80, 80), style)
                .setClickAction(button -> manager.setState(GameState.MENU)));
    }

    @Override
    public void update(double time) {

    }

    @Override
    public void render(Graphics2D g) {
        g.setColor(Color.WHITE);
        g.setFont(Fontf.getFont("MinimalPixel").deriveFont(Font.PLAIN, 80));

        g.drawString("Options", 630, 140);
    }

    @Override
    public void input(@NonNull MouseHandler handler, @NonNull KeyboardHandler keyboardHandler) {

    }
}
