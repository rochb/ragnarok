package me.sharkz.ragnarok.game.states;

import lombok.NonNull;
import me.sharkz.ragnarok.Ragnarok;
import me.sharkz.ragnarok.inputs.KeyboardHandler;
import me.sharkz.ragnarok.inputs.MouseHandler;
import me.sharkz.ragnarok.ui.ButtonManager;

import java.awt.*;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PlayState implements State {

    public PlayState(@NonNull Ragnarok ragnarok) {
        ButtonManager manager = ragnarok.getButtonManager();

    }

    @Override
    public void update(double time) {

    }

    @Override
    public void render(Graphics2D g) {

    }

    @Override
    public void input(@NonNull MouseHandler handler, @NonNull KeyboardHandler keyboardHandler) {

    }
}
