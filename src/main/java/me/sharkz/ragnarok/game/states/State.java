package me.sharkz.ragnarok.game.states;

import lombok.NonNull;
import me.sharkz.ragnarok.inputs.KeyboardHandler;
import me.sharkz.ragnarok.inputs.MouseHandler;

import java.awt.*;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface State {

    void update(double time);

    void render(Graphics2D g);

    void input(@NonNull MouseHandler handler, @NonNull KeyboardHandler keyboardHandler);
}
