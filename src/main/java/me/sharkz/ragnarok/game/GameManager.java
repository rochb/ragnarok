package me.sharkz.ragnarok.game;

import lombok.Getter;
import lombok.NonNull;
import me.sharkz.ragnarok.Ragnarok;
import me.sharkz.ragnarok.game.states.*;
import me.sharkz.ragnarok.window.BoardSize;

import java.awt.*;
import java.awt.image.BufferStrategy;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class GameManager {

    /* State */
    private GameState state;
    private State currentState;

    /* Runnable */
    private GameRunnable runnable;

    /* Thread */
    private Thread thread;

    /* Ragnarok */
    private final Ragnarok ragnarok;

    public GameManager(@NonNull Ragnarok ragnarok) {
        this.ragnarok = ragnarok;
        setState(GameState.MENU);
    }

    public void start(@NonNull BoardSize size, @NonNull BufferStrategy bufferStrategy) {
        this.runnable = new GameRunnable(ragnarok, this, size, bufferStrategy);
        this.thread = new Thread(runnable, "Ragnarok");
        this.thread.start();
    }

    public void update(double time) {
        currentState.update(time);
        ragnarok.update(time);
    }

    public void render(@NonNull Graphics2D graphics) {
        graphics.setColor(new Color(33, 30, 39));
        graphics.fillRect(0, 0, runnable.getDimension().getWidth(), runnable.getDimension().getWidth());

        currentState.render(graphics);
        ragnarok.render(graphics);
    }

    public void input() {
        ragnarok.input();
        currentState.input(ragnarok.getMouseHandler(), ragnarok.getKeyboardHandler());
    }

    /* State */
    public boolean is(@NonNull GameState state) {
        return this.state.equals(state);
    }

    public void setState(@NonNull GameState state) {
        if (this.state == state)
            return;
        ragnarok.getButtonManager()
                .getButtons()
                .clear();
        ragnarok.getKeyboardHandler()
                .getControls()
                .clear();
        switch (state) {
            case MENU:
                currentState = new MenuState(ragnarok, this);
                break;
            case OPTIONS:
                currentState = new OptionState(ragnarok, this);
                break;
            case PLAYING:
                currentState = new PlayState(ragnarok);
                break;
            case EASTER_EGG:
                currentState = new EasterEggState(ragnarok, this);
                break;
        }
        this.state = state;
    }
}
