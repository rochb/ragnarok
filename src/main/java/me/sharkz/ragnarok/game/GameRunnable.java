package me.sharkz.ragnarok.game;

import lombok.Getter;
import lombok.NonNull;
import me.sharkz.ragnarok.Ragnarok;
import me.sharkz.ragnarok.window.BoardSize;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class GameRunnable implements Runnable {

    /* Ragnarok */
    private final Ragnarok ragnarok;

    /* Game */
    private final GameManager manager;

    /* Size */
    @Getter
    private final BoardSize dimension;

    /* State */
    private boolean running = false;

    /* Frames */
    private int oldFrameCount;
    private int oldTickCount;

    /* Ticks */
    private int tickCount;

    /* Image */
    private BufferedImage img;

    /* Graphics */
    private Graphics2D g;

    /* Buffer Strategy */
    private final BufferStrategy bs;

    public GameRunnable(@NonNull Ragnarok ragnarok, @NonNull GameManager manager, @NonNull BoardSize dimension, @NonNull BufferStrategy bs) {
        this.ragnarok = ragnarok;
        this.manager = manager;
        this.dimension = dimension;
        this.bs = bs;
    }

    @Override
    public void run() {
        initialize();

        final double GAME_HERTZ = 64.0;
        final double TBU = 1000000000 / GAME_HERTZ; // Time Before Update

        final int MUBR = 3; // Must Update before render

        double lastUpdateTime = System.nanoTime();
        double lastRenderTime;

        final double TARGET_FPS = 120;
        final double TTBR = 1000000000 / TARGET_FPS; // Total time before render

        int frameCount = 0;
        int lastSecondTime = (int) (lastUpdateTime / 1000000000);
        oldFrameCount = 0;

        tickCount = 0;
        oldTickCount = 0;

        while (running) {
            double now = System.nanoTime();
            int updateCount = 0;
            while (((now - lastUpdateTime) > TBU) && (updateCount < MUBR)) {
                manager.update(now);
                manager.input();
                lastUpdateTime += TBU;
                updateCount++;
                tickCount++;
                // (^^^^) We use this varible for the soul purpose of displaying it
            }

            if ((now - lastUpdateTime) > TBU)
                lastUpdateTime = now - TBU;

            manager.input();
            manager.render(g);
            draw();
            lastRenderTime = now;
            frameCount++;

            int thisSecond = (int) (lastUpdateTime / 1000000000);
            if (thisSecond > lastSecondTime) {
                if (frameCount != oldFrameCount)
                    //System.out.println("NEW SECOND " + thisSecond + " " + frameCount);
                    oldFrameCount = frameCount;

                if (tickCount != oldTickCount)
                    //System.out.println("NEW SECOND (T) " + thisSecond + " " + tickCount);
                    oldTickCount = tickCount;
                tickCount = 0;
                frameCount = 0;
                lastSecondTime = thisSecond;
            }

            while (now - lastRenderTime < TTBR && now - lastUpdateTime < TBU) {
                Thread.yield();

                try {
                    Thread.sleep(1);
                } catch (Exception e) {
                    System.out.println("ERROR: yielding thread");
                }

                now = System.nanoTime();
            }

        }
    }

    public void initialize() {
        running = true;

        img = new BufferedImage(dimension.getWidth(), dimension.getHeight(), BufferedImage.TYPE_INT_ARGB);
        g = (Graphics2D) img.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    }

    public void draw() {
        do {
            Graphics g2 = bs.getDrawGraphics();
            g2.drawImage(img, 0, 0, dimension.getWidth(), dimension.getHeight(), null);
            g2.dispose();
            bs.show();
        } while (bs.contentsLost());
    }
}
