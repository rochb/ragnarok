package me.sharkz.ragnarok.window;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Setter
@Getter
@AllArgsConstructor
public class BoardSize {

    private int width;
    private int height;
}
