package me.sharkz.ragnarok.window;

import lombok.Getter;
import lombok.NonNull;
import me.sharkz.ragnarok.Ragnarok;
import me.sharkz.ragnarok.game.GameManager;

import javax.swing.*;
import java.awt.image.BufferStrategy;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class GameBoard extends JPanel {

    private final BufferStrategy bufferStrategy;
    private final BoardSize dim;
    private final GameManager manager;

    public GameBoard(@NonNull Ragnarok ragnarok, @NonNull BufferStrategy bufferStrategy, @NonNull BoardSize size) {
        this.bufferStrategy = bufferStrategy;
        this.dim = size;
        this.manager = ragnarok.getManager();

        /* Inputs */
        addMouseListener(ragnarok.getMouseHandler());
        addMouseMotionListener(ragnarok.getMouseHandler());
        addKeyListener(ragnarok.getKeyboardHandler());

        /* Misc */
        setFocusable(true);
        requestFocus();
    }

    @Override
    public void addNotify() {
        super.addNotify();

        if (Objects.isNull(manager.getRunnable()))
            manager.start(dim, bufferStrategy);
    }
}
