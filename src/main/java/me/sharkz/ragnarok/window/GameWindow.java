package me.sharkz.ragnarok.window;

import lombok.Getter;
import lombok.NonNull;
import me.sharkz.ragnarok.Ragnarok;
import me.sharkz.ragnarok.utils.Resources;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class GameWindow extends JFrame {

    private final Ragnarok ragnarok;

    public GameWindow(@NonNull Ragnarok ragnarok) throws HeadlessException {
        super("Ragnarok - 1.1");
        this.ragnarok = ragnarok;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setIgnoreRepaint(true);
        pack();

        BufferedImage icon = Resources.getImage("icon.png");
        setIconImage(icon);
        Taskbar taskbar = Taskbar.getTaskbar();
        try {
            taskbar.setIconImage(icon);
        } catch (final UnsupportedOperationException | SecurityException ignored) {
        }
        setSize(1422, 800);
        setMinimumSize(new Dimension(533, 300));
        setLocationRelativeTo(null);
        setVisible(true);

        addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent evt) {
                Component c = (Component) evt.getSource();
                if (Objects.nonNull(ragnarok.getManager().getRunnable())) {
                    ragnarok.getManager().getRunnable().getDimension().setWidth(c.getWidth());
                    ragnarok.getManager().getRunnable().getDimension().setHeight(c.getHeight());
                }
            }
        });
    }

    public void addNotify() {
        super.addNotify();

        createBufferStrategy(4);
        BufferStrategy bs = getBufferStrategy();

        setContentPane(new GameBoard(ragnarok, bs, new BoardSize(1422, 800)));
    }

}
