package me.sharkz.ragnarok;

import lombok.Getter;
import lombok.NonNull;
import me.sharkz.ragnarok.game.GameManager;
import me.sharkz.ragnarok.graphics.fonts.FontManager;
import me.sharkz.ragnarok.graphics.fonts.Fontf;
import me.sharkz.ragnarok.inputs.KeyboardHandler;
import me.sharkz.ragnarok.inputs.MouseHandler;
import me.sharkz.ragnarok.ui.ButtonManager;
import me.sharkz.ragnarok.window.GameWindow;

import java.awt.*;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class Ragnarok {

    private static Ragnarok instance;

    /* JFrame */
    private final GameWindow window;

    /* Game */
    private final GameManager manager;

    /* Inputs */
    private final KeyboardHandler keyboardHandler;
    private final MouseHandler mouseHandler;

    /* Fonts */
    private final FontManager fontManager;

    /* Buttons */
    private final ButtonManager buttonManager;

    public Ragnarok(String[] args) {
        instance = this;

        /* Fonts */
        this.fontManager = new FontManager();
        Fontf.loadFont("MinimalPixel.ttf", "MinimalPixel");

        /* Inputs */
        this.mouseHandler = new MouseHandler();
        this.keyboardHandler = new KeyboardHandler(this);

        /* Buttons */
        this.buttonManager = new ButtonManager(this);

        /* Game */
        this.manager = new GameManager(this);

        /* JFrame */
        this.window = new GameWindow(this);
    }

    public void update(double time) {
        buttonManager.update();
    }

    public void input() {
        buttonManager.input();
    }

    public void render(@NonNull Graphics2D graphics) {
        buttonManager.render(graphics);
    }

    public static Ragnarok get() {
        return instance;
    }
}
