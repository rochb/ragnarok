package me.sharkz.ragnarok.math;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AABB {

    private final Vector2f position;
    private final float width;
    private final float height;
    private final float size;
    private final float radius;
    private final float surfaceArea;

    private float xOffset;
    private float yOffset;

    public AABB(Vector2f position, float w, float h) {
        this.position = position;
        this.width = w;
        this.height = h;
        this.surfaceArea = w * h;
        this.radius = 0;
        this.size = Math.max(w, h);
    }

    public AABB(Vector2f position, float radius) {
        this.position = position;
        this.radius = radius;
        this.surfaceArea = (float) Math.PI * (radius * radius);
        this.width = 0;
        this.height = 0;
        this.size = radius;
    }

    public boolean collides(AABB bBox) {
        return collides(0, 0, bBox);
    }

    public boolean collides(float dx, float dy, AABB bBox) {
        float ax = ((position.getX() + (xOffset)) + (this.width / 2)) + dx;
        float ay = ((position.getY() + (yOffset)) + (this.height / 2)) + dy;
        float bx = ((bBox.getPosition().getX() + (bBox.getXOffset())) + (bBox.getWidth() / 2));
        float by = ((bBox.getPosition().getY() + (bBox.getYOffset())) + (bBox.getHeight() / 2));

        if (Math.abs(ax - bx) < (this.width / 2) + (bBox.getWidth() / 2))
            return Math.abs(ay - by) < (this.height / 2) + (bBox.getHeight() / 2);
        return false;
    }

    public boolean inside(int xp, int yp) {
        if (xp == -1 || yp == -1) return false;

        int wTemp = (int) this.width;
        int hTemp = (int) this.height;
        int x = this.position.getX();
        int y = this.position.getY();

        if (xp < x || yp < y)
            return false;

        wTemp += x;
        hTemp += y;
        return ((wTemp < x || wTemp > xp) && (hTemp < y || hTemp > yp));
    }

    public boolean intersect(AABB aBox) {
        if ((position.getY() + xOffset > aBox.position.getX() + aBox.getXOffset() + aBox.getSize())
                || (aBox.position.getY() + xOffset > position.getX() + aBox.getXOffset() + aBox.getSize()))
            return false;


        return (!(position.getY() + yOffset > aBox.getPosition().getY() + aBox.getYOffset() + aBox.getSize()))
                && (!(aBox.getPosition().getY() + yOffset > position.getY() + aBox.getYOffset() + aBox.getSize()));
    }

    public boolean colCircle(AABB circle) {
        float totalRadius = radius + circle.getRadius();
        totalRadius *= totalRadius;

        float dx = (position.getX() + circle.getPosition().getX());
        float dy = (position.getY() + circle.getPosition().getY());

        return totalRadius < (dx * dx) + (dy * dy);
    }

    public boolean colCircleBox(AABB aBox) {
        float dx = Math.max(aBox.getPosition().getX() + aBox.getXOffset(), Math.min(position.getX() + (radius / 2), aBox.getPosition().getX() + aBox.getXOffset() + aBox.getWidth()));
        float dy = Math.max(aBox.getPosition().getY() + aBox.getYOffset(), Math.min(position.getY() + (radius / 2), aBox.getPosition().getY() + aBox.getYOffset() + aBox.getHeight()));

        dx = position.getX() + (radius / 2) - dx;
        dy = position.getY() + (radius / 2) - dy;

        return Math.sqrt(dx * dx + dy * dy) < radius / 2;
    }

    public float distance(Vector2f other) {
        float dx = position.getX() - other.getX();
        float dy = position.getY() - other.getY();
        return (float) Math.sqrt(dx * dx + dy * dy);
    }

    public AABB merge(AABB other) {
        int maxW = (int) Math.max(width, other.getWidth());
        int maxH = (int) Math.max(height, other.getHeight());
        return new AABB(position, maxW, maxH);
    }

    public String toString() {
        String x = Float.toString(position.getX());
        String y = Float.toString(position.getY());
        String w = Float.toString(this.width);
        String h = Float.toString(this.height);
        return "{" + x + ", " + y + " : " + w + ", " + h + "}";
    }


}
