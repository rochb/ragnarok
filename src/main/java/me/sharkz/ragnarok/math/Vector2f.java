package me.sharkz.ragnarok.math;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
@AllArgsConstructor
@Setter
public class Vector2f {

    private int x;
    private int y;

    public void increment(@NonNull Vector2f vector2f) {
        this.x += vector2f.getX();
        this.y += vector2f.getY();
    }

    public void decrement(@NonNull Vector2f vector2f) {
        this.x -= vector2f.getX();
        this.y -= vector2f.getY();
    }

    public void multiply(@NonNull Vector2f vector2f) {
        this.x *= vector2f.getX();
        this.y *= vector2f.getY();
    }

    public void divide(@NonNull Vector2f vector2f) {
        this.x /= vector2f.getX();
        this.y /= vector2f.getY();
    }

    public void increment(@NonNull int x, int y) {
        this.x += x;
        this.y += y;
    }

    public void decrement(@NonNull int x, int y) {
        this.x -= x;
        this.y -= y;
    }

    public void multiply(@NonNull int x, int y) {
        this.x *= x;
        this.y *= y;
    }

    public void divide(@NonNull int x, int y) {
        this.x /= x;
        this.y /= y;
    }

}
