package me.sharkz.ragnarok.level.loader;

import lombok.NonNull;
import me.sharkz.ragnarok.level.Level;

import java.io.FileNotFoundException;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface LevelLoader {

    Optional<Level> load(@NonNull String fileName) throws FileNotFoundException;

}
