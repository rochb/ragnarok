package me.sharkz.ragnarok.level.loader;

import lombok.NonNull;
import me.sharkz.ragnarok.graphics.sprite.Sprite;
import me.sharkz.ragnarok.graphics.sprite.SpriteSheet;
import me.sharkz.ragnarok.level.Level;
import me.sharkz.ragnarok.level.tile.map.NormalTileMap;
import me.sharkz.ragnarok.level.tile.map.ObjectTileMap;
import me.sharkz.ragnarok.level.tile.map.TileMap;
import me.sharkz.ragnarok.utils.Resources;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class XMLLevelLoader implements LevelLoader {

    private static final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    @Override
    public Optional<Level> load(@NonNull String fileName) throws FileNotFoundException {
        String imagePath = null;
        int tileWidth = 0, tileHeight = 0, columns = 0;
        Document map = null;
        List<TileMap> tileMaps = new ArrayList<>();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();

            //Format map file
            map = builder.parse(Resources.getLevelFile(fileName));
            map.getDocumentElement().normalize();

            Element mapElement = getElement(map, "tileset");

            //Format tileset file
            Document tileset = builder.parse(Resources.getTileSetFile(mapElement.getAttribute("source")));
            tileset.getDocumentElement().normalize();

            //Get elements from 3 files
            Element tilesetElement = getElement(tileset, "tileset");
            Element imgElement = getElement(tileset, "image");

            // Define value from file 's values
            imagePath = Resources.TILESET_PATH + "images/" + imgElement.getAttribute("source");
            tileWidth = Integer.parseInt(tilesetElement.getAttribute("tilewidth"));
            tileHeight = Integer.parseInt(tilesetElement.getAttribute("tileheight"));
            columns = Integer.parseInt(tilesetElement.getAttribute("columns"));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        //  Load sprite
        if (imagePath == null || getClass().getClassLoader().getResource(imagePath) == null)
            throw new FileNotFoundException(String.format("Cannot load %s because tileset image file is inaccessible.", fileName));
        SpriteSheet spriteSheet = new SpriteSheet(new Sprite(Resources.getImage(imagePath)), tileWidth, tileHeight);

        NodeList list = map.getElementsByTagName("layer");
        int layers = list.getLength();
        int width = 0;
        int height = 0;
        String[] data = new String[10];

        for (int i = 0; i < layers; i++) {
            Node node = list.item(i);
            Element e = (Element) node;
            if (i <= 0) {
                width = Integer.parseInt(e.getAttribute("width"));
                height = Integer.parseInt(e.getAttribute("height"));
            }

            data[i] = e.getElementsByTagName("data").item(0).getTextContent();
            if (i >= 1)
                tileMaps.add(new NormalTileMap(data[i], spriteSheet, width, height, tileWidth, tileHeight, columns));
            else
                tileMaps.add(new ObjectTileMap(data[i], spriteSheet, width, height, tileWidth, tileHeight, columns));
        }
        Level level = new Level(fileName, tileWidth, tileHeight, columns, width, height);
        level.getTileMaps().addAll(tileMaps);
        return Optional.of(level);
    }

    private Element getElement(Document document, String key) {
        return (Element) document.getElementsByTagName(key).item(0);
    }
}
