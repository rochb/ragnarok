package me.sharkz.ragnarok.level.tile;

import me.sharkz.ragnarok.math.AABB;
import me.sharkz.ragnarok.math.Vector2f;

import java.awt.image.BufferedImage;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class HoleTile extends Tile {

    public HoleTile(BufferedImage image, Vector2f position, int width, int height) {
        super(image, position, width, height);
    }

    @Override
    public boolean update(AABB p) {
        return false;
    }

    @Override
    public boolean isInside(AABB p) {
        return (!(p.getPosition().getX() + p.getXOffset() < getPosition().getX()))
                && (!(p.getPosition().getY() + p.getYOffset() < getPosition().getY()))
                && (!(getWidth() + getPosition().getX() < p.getWidth() + (p.getPosition().getX() + p.getXOffset())))
                && (!(getHeight() + getPosition().getY() < p.getHeight() + (p.getPosition().getY() + p.getYOffset())));
    }
}
