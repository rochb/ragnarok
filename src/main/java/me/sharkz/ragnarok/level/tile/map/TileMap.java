package me.sharkz.ragnarok.level.tile.map;

import lombok.Data;
import lombok.NonNull;
import me.sharkz.ragnarok.graphics.sprite.SpriteSheet;
import me.sharkz.ragnarok.level.tile.Tile;
import me.sharkz.ragnarok.math.AABB;

import java.awt.*;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public abstract class TileMap {

    protected final Tile[] tiles;
    protected final int tileWidth;
    protected final int tileHeight;
    protected final int width;
    protected final int height;

    public TileMap(@NonNull String data, @NonNull SpriteSheet sprite, @NonNull int width, @NonNull int height, @NonNull int tileWidth, @NonNull int tileHeight, @NonNull int tileColumns) {
        this.tiles = new Tile[width * height];
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
        this.width = width;
        this.height = height;
    }

    public abstract void render(Graphics2D g, AABB cam);
}
