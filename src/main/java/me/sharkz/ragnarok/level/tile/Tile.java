package me.sharkz.ragnarok.level.tile;

import lombok.Data;
import me.sharkz.ragnarok.math.AABB;
import me.sharkz.ragnarok.math.Vector2f;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public abstract class Tile {

    private final BufferedImage image;
    private final Vector2f position;
    private final int width;
    private final int height;

    public abstract boolean update(AABB p);

    public abstract boolean isInside(AABB p);

    public void render(Graphics2D g) {
        g.drawImage(image, position.getX(), position.getY(), width, height, null);
    }
}
