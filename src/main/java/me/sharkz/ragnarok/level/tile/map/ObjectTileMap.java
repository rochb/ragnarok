package me.sharkz.ragnarok.level.tile.map;

import lombok.NonNull;
import me.sharkz.ragnarok.graphics.sprite.SpriteSheet;
import me.sharkz.ragnarok.level.tile.HoleTile;
import me.sharkz.ragnarok.level.tile.ObjectTile;
import me.sharkz.ragnarok.level.tile.Tile;
import me.sharkz.ragnarok.math.AABB;
import me.sharkz.ragnarok.math.Vector2f;

import java.awt.*;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ObjectTileMap extends TileMap {

    public ObjectTileMap(@NonNull String data, @NonNull SpriteSheet sprite, @NonNull int width, @NonNull int height, @NonNull int tileWidth, @NonNull int tileHeight, @NonNull int tileColumns) {
        super(data, sprite, width, height, tileWidth, tileHeight, tileColumns);

        Tile tmpTile;
        String[] block = data.split(",");
        for (int i = 0; i < (width * height); i++) {
            int temp = Integer.parseInt(block[i].replaceAll("\\s+", ""));
            if (temp == 0)
                continue;
            if (temp == 172)
                //TODO: find edge and connect them to form one polygon
                tmpTile = new HoleTile(sprite.getSprite((temp - 1) % tileColumns, (temp - 1) / tileColumns).getImage(), new Vector2f((i % width) * tileWidth, (i / height) * tileHeight), tileWidth, tileHeight);
            else
                tmpTile = new ObjectTile(sprite.getSprite((temp - 1) % tileColumns, (temp - 1) / tileColumns).getImage(), new Vector2f((i % width) * tileWidth, (i / height) * tileHeight), tileWidth, tileHeight);
            tiles[i] = tmpTile;
        }
    }


    @Override
    public void render(Graphics2D g, AABB cam) {
        int x = (int) ((cam.getPosition().getX()) / tileWidth);
        int y = (int) ((cam.getPosition().getY()) / tileHeight);

        for (int i = x; i < x + (cam.getWidth() / tileWidth); i++) {
            for (int j = y; j < y + (cam.getHeight() / tileHeight); j++) {
                if (i + (j * height) > -1 && i + (j * height) < tiles.length && Objects.nonNull(tiles[i + (j * height)]))
                    tiles[i + (j * height)].render(g);
            }
        }
    }
}
