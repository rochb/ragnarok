package me.sharkz.ragnarok.level;

import lombok.Data;
import me.sharkz.ragnarok.level.tile.NormalTile;
import me.sharkz.ragnarok.level.tile.map.TileMap;

import java.awt.*;
import java.util.ArrayList;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class Level {

    private final String name;
    private final int tileWidth;
    private final int tileHeight;
    private final int columns;
    private final int width;
    private final int height;
    private final ArrayList<TileMap> tileMaps = new ArrayList<>();

    public NormalTile[] getNormalTile(int id) {
        int normMap = 1;
        if (tileMaps.size() < 2) normMap = 0;
        NormalTile[] block = new NormalTile[9];

        int i = 0;
        for (int x = 1; x > -2; x--) {
            for (int y = 1; y > -2; y--) {
                if (id + (y + x * height) < 0 || id + (y + x * height) > (width * height) - 2) continue;
                block[i] = (NormalTile) tileMaps.get(normMap).getTiles()[id + (y + x * height)];
                i++;
            }
        }

        return block;
    }

    public void render(Graphics2D g) {
        // if (camera == null) return;
        tileMaps.forEach(tileMap -> {
            tileMap.render(g, camera.getBounds());
        });
    }
}
