package me.sharkz.ragnarok.inputs.keys;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface KeyControl {

    void onClick();

    void onRelease();

    int getKey();

}
