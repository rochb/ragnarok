package me.sharkz.ragnarok.inputs.keys;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum KeyState {
    PRESSED,
    RELEASED
}
