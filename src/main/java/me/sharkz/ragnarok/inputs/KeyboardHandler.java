package me.sharkz.ragnarok.inputs;

import lombok.Getter;
import lombok.NonNull;
import me.sharkz.ragnarok.Ragnarok;
import me.sharkz.ragnarok.inputs.keys.KeyControl;
import me.sharkz.ragnarok.inputs.keys.KeyState;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class KeyboardHandler implements KeyListener {

    @Getter
    private final Map<KeyControl, KeyState> controls = new HashMap<>();

    public KeyboardHandler(@NonNull Ragnarok ragnarok) {
    }

    public void register(@NonNull KeyControl... controls) {
        for (KeyControl control : controls)
            register(control);
    }

    public void register(@NonNull KeyControl control) {
        controls.put(control, KeyState.RELEASED);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        new HashMap<>(controls).entrySet()
                .stream()
                .filter(en -> en.getKey().getKey() == e.getKeyCode())
                .filter(en -> !en.getValue().equals(KeyState.PRESSED))
                .map(Map.Entry::getKey)
                .peek(control -> controls.replace(control, KeyState.PRESSED))
                .forEach(KeyControl::onClick);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        new HashMap<>(controls).keySet()
                .stream()
                .filter(keyControl -> keyControl.getKey() == e.getKeyCode())
                .peek(control -> controls.replace(control, KeyState.RELEASED))
                .forEach(KeyControl::onClick);
    }
}
