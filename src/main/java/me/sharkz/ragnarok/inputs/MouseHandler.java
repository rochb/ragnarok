package me.sharkz.ragnarok.inputs;

import lombok.Getter;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class MouseHandler implements MouseListener, MouseMotionListener {

    private int x = -1;
    private int y = -1;
    private int b = -1;

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        b = e.getButton();
        System.out.println(e.getX() + " " + e.getY());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        b = -1;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        setPosition(e);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        setPosition(e);
    }

    public boolean isButtonPressed() {
        return b != -1;
    }

    public boolean isLeftPressed() {
        return b == 1;
    }

    public boolean isRightPressed() {
        return b == 3;
    }

    private void setPosition(MouseEvent e) {
        this.x = e.getX();
        this.y = e.getY();
    }
}
