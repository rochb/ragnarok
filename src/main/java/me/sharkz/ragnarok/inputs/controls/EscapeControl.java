package me.sharkz.ragnarok.inputs.controls;

import lombok.RequiredArgsConstructor;
import me.sharkz.ragnarok.game.GameManager;
import me.sharkz.ragnarok.game.GameState;
import me.sharkz.ragnarok.inputs.keys.KeyControl;

import java.awt.event.KeyEvent;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
public class EscapeControl implements KeyControl {

    private final GameManager manager;
    private final GameState state;

    @Override
    public void onClick() {
        manager.setState(state);
    }

    @Override
    public void onRelease() {

    }

    @Override
    public int getKey() {
        return KeyEvent.VK_ESCAPE;
    }
}
