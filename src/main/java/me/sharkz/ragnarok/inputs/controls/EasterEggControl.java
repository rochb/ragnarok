package me.sharkz.ragnarok.inputs.controls;

import lombok.RequiredArgsConstructor;
import me.sharkz.ragnarok.game.GameManager;
import me.sharkz.ragnarok.game.GameState;
import me.sharkz.ragnarok.inputs.keys.KeyControl;

import java.awt.event.KeyEvent;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
public class EasterEggControl implements KeyControl {

    private final GameManager manager;

    @Override
    public void onClick() {
        manager.setState(GameState.EASTER_EGG);
    }

    @Override
    public void onRelease() {

    }

    @Override
    public int getKey() {
        return KeyEvent.VK_F7;
    }
}
