package me.sharkz.ragnarok.ui;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum ButtonState {
    PRESSED,
    NORMAL,
    HOVERED,

}
