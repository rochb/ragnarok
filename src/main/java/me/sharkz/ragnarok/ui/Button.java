package me.sharkz.ragnarok.ui;

import lombok.Data;
import lombok.NonNull;
import lombok.Setter;
import me.sharkz.ragnarok.graphics.fonts.Fontf;
import me.sharkz.ragnarok.math.AABB;
import me.sharkz.ragnarok.math.Vector2f;
import me.sharkz.ragnarok.utils.ImageUtils;

import java.awt.*;
import java.awt.font.TextLayout;
import java.awt.image.BufferedImage;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
@Setter
public class Button {

    private String text;
    private BufferedImage icon;
    private final Vector2f position;
    private final ButtonStyle style;
    private final ButtonSize size;

    private AABB bounds;
    private ButtonState state = ButtonState.NORMAL;
    private boolean enabled = true;
    private Consumer<Button> onClick;

    public Button(@NonNull String text, @NonNull ButtonSize size, @NonNull Vector2f position, @NonNull ButtonStyle style) {
        this.text = text;
        this.position = position;
        this.style = style;
        this.size = size;
        updateBounds();
    }

    public Button(@NonNull BufferedImage icon, @NonNull ButtonSize size, @NonNull Vector2f position, @NonNull ButtonStyle style) {
        this.icon = icon;
        this.position = position;
        this.style = style;
        this.size = size;
        updateBounds();
    }

    public void updateBounds() {
        BufferedImage image = style.get(state);
        this.bounds = new AABB(position, size.getWidth(), size.getHeight());
    }

    public void render(@NonNull Graphics2D g) {
        Image image = style.get(state);
        if (!enabled)
            image = ImageUtils.grayscale(image);
        g.drawImage(image, position.getX(), position.getY(), size.getWidth(), size.getHeight(), null);

        /* Label */
        if (Objects.nonNull(text) && !text.isEmpty()) {
            g.setColor(Color.WHITE);
            g.setFont(Fontf.getFont("MinimalPixel"));
            TextLayout textLayout = new TextLayout(text, g.getFont(),
                    g.getFontRenderContext());
            double textHeight = textLayout.getBounds().getHeight();
            double textWidth = textLayout.getBounds().getWidth();

            g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

            g.drawString(text, position.getX() + (size.getWidth() / 2 - (int) textWidth / 2),
                    position.getY() + (size.getHeight() / 2 + (int) textHeight / 2));
            return;
        }
        g.drawImage(icon, position.getX() + (size.getWidth() / 2 - icon.getWidth() / 2),
                position.getY() + (size.getHeight() / 2 - icon.getHeight() / 2) - (size.getHeight() / 20), null);
    }

    public Button setClickAction(Consumer<Button> onClick) {
        this.onClick = onClick;
        return this;
    }
}
