package me.sharkz.ragnarok.ui;

import lombok.Data;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class ButtonSize {

    private final int width;
    private final int height;
}
