package me.sharkz.ragnarok.ui;

import lombok.Getter;
import lombok.NonNull;
import me.sharkz.ragnarok.Ragnarok;
import me.sharkz.ragnarok.inputs.MouseHandler;
import me.sharkz.ragnarok.utils.Resources;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ButtonManager {

    @Getter
    private final List<Button> buttons = new ArrayList<>();
    private final List<ButtonStyle> styles = new ArrayList<>();
    private final MouseHandler mouseHandler;

    public ButtonManager(@NonNull Ragnarok ragnarok) {
        this.mouseHandler = ragnarok.getMouseHandler();

        /* Styles */
        registerStyle(new ButtonStyle("large", Resources.getButton("large.png"), 46, 15));
        registerStyle(new ButtonStyle("small", Resources.getButton("small.png"), 30, 15));
        registerStyle(new ButtonStyle("square", Resources.getButton("square.png"), 14, 14));
    }

    public void registerStyle(@NonNull ButtonStyle style) {
        this.styles.add(style);
    }

    public void render(@NonNull Graphics2D g) {
        buttons.forEach(button -> button.render(g));
    }

    public void update() {

    }

    public void input() {
        new ArrayList<>(buttons).stream()
                .filter(Objects::nonNull)
                .filter(Button::isEnabled)
                .forEach(button -> {
                    if (button.getBounds().inside(mouseHandler.getX(), mouseHandler.getY())) {
                        if (mouseHandler.isLeftPressed() && !button.getState().equals(ButtonState.PRESSED)) {
                            button.setState(ButtonState.PRESSED);
                            if (Objects.nonNull(button.getOnClick()))
                                button.getOnClick().accept(button);
                        } else if (!mouseHandler.isButtonPressed() && button.getState().equals(ButtonState.PRESSED))
                            button.setState(ButtonState.NORMAL);
                        if (button.getState().equals(ButtonState.NORMAL))
                            button.setState(ButtonState.HOVERED);
                    } else if (button.getState().equals(ButtonState.HOVERED))
                        button.setState(ButtonState.NORMAL);
                });
    }

    public void add(@NonNull Button button) {
        this.buttons.add(button);
    }

    public void remove(@NonNull Button button) {
        this.buttons.remove(button);
    }

    public Optional<ButtonStyle> getByName(@NonNull String name) {
        return styles.stream()
                .filter(style -> style.getName().equalsIgnoreCase(name))
                .findFirst();
    }


}
