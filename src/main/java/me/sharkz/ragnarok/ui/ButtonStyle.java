package me.sharkz.ragnarok.ui;

import lombok.Data;
import lombok.NonNull;
import me.sharkz.ragnarok.graphics.sprite.Sprite;
import me.sharkz.ragnarok.math.Vector2f;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class ButtonStyle {

    private final String name;
    private final Map<ButtonState, BufferedImage> images = new HashMap<>();

    public ButtonStyle(@NonNull String name, @NonNull BufferedImage image, @NonNull int width, @NonNull int height) {
        this.name = name;

        Sprite sprite = new Sprite(image);
        Vector2f position = new Vector2f(0, 0);
        for (ButtonState state : ButtonState.values()) {
            images.put(state, sprite.getNew(position.getX(), position.getY(), width, height).getImage());
            position.increment(0, height);
            if (position.getX() + width > sprite.getWidth()
                    || position.getY() + height > sprite.getHeight())
                break;
        }
    }

    public BufferedImage get(@NonNull ButtonState state) {
        return images.get(state);
    }
}
