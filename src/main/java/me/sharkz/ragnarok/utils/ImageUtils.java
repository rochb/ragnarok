package me.sharkz.ragnarok.utils;

import lombok.NonNull;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ImageUtils {

    public static BufferedImage resize(@NonNull BufferedImage image, int width, int height) {
        Image temp = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = result.createGraphics();
        g.drawImage(temp, 0, 0, null);
        g.dispose();
        return result;
    }

    public static Image grayscale(@NonNull Image image) {
        ImageFilter filter = new GrayFilter(true, 2);
        ImageProducer producer = new FilteredImageSource(image.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(producer);
    }
}
