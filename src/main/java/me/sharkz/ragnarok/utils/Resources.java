package me.sharkz.ragnarok.utils;

import lombok.NonNull;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class Resources {

    public static final String FONTS_PATH = "ui/fonts/";
    public static final String BUTTONS_PATH = "ui/buttons/";
    public static final String ICONS_PATH = "ui/icons/";
    public static final String LEVEL_PATH = "lvl/";
    public static final String TILESET_PATH = "tilesets/";

    public static InputStream getLevelFile(@NonNull String path) {
        return getFile(LEVEL_PATH + path);
    }

    public static InputStream getTileSetFile(@NonNull String path) {
        return getFile(TILESET_PATH + path);
    }

    public static BufferedImage getFont(@NonNull String path) {
        return getImage(FONTS_PATH + path);
    }

    public static BufferedImage getIcon(@NonNull String path) {
        return getImage(ICONS_PATH + path);
    }

    public static InputStream getFontFile(@NonNull String path) {
        return getFile(FONTS_PATH + path);
    }

    public static InputStream getFile(@NonNull String path) {
        return Resources.class.getClassLoader().getResourceAsStream(path);
    }

    public static BufferedImage getButton(@NonNull String path) {
        return getImage(BUTTONS_PATH + path);
    }

    public static BufferedImage getImage(@NonNull String path) {
        URL url = getURL(path);
        try {
            return ImageIO.read(url);
        } catch (IOException e) {
            String fmt = "cannot read resource [%s] at [%s]";
            String err = String.format(fmt, path, url);
            throw new IllegalStateException(err, e);
        }
    }

    public static URL getURL(@NonNull String path) {
        URL url = Resources.class.getClassLoader().getResource(path);
        if (url == null) {
            String fmt = "cannot find resource [%s]";
            throw new IllegalStateException(String.format(fmt, path));
        }
        return url;
    }
}
